package org.example.transfer;

public class BasicContinue {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            if (i == 2) {
                continue; // skipping current iteration
            }
            System.out.println(i);
        }
    }
}

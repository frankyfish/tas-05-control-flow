package org.example.iteration;

class BasicWhile {
    public static void main(String[] args) {
        int i = 0;
        while (i < 3) {
            System.out.println("i=" + i);
            i++;
        }
        System.out.println("finished!");
    }
}

class BlocklessWhile {
    public static void main(String[] args) {
        // what happens?
        short count = 0;
        while (count < 10)
            System.out.println("count=" + count);
            count++;
        System.out.println("done!count=" + count);
    }
}

package org.example.selection;

class BasicSwitch {
    public static void main(String[] args) {
        int answer = 42;

        switch (answer) {
            case 404:
                System.out.println("not found");
                break;
            case 418:
                System.out.println("i'm a teapot");
                break;
            case 38: // no statement - OK
            default:
                System.out.println("Life, the Universe and Everything");
        }

        System.out.println("done!");
    }
}

class AnotherSwitch {

    public static void main(String[] args) {
        // what is the output?
        String greeting = "Hi, %s ! \n";

        String name = "John";
        switch (name) {
            case "Paul":
                System.out.printf(greeting, "Paul");
            case "John":
                System.out.printf(greeting, "John");
            default:
                System.out.printf(greeting, "human");
        }

        System.out.println("done!");
    }

}

class ConstantSwitch {
    public static void main(String[] args) {
//        String predefined = "one";
//        switch (predefined) {
//            case args[0]:
//                System.out.println("MATCHED!");
//        }
    }
}

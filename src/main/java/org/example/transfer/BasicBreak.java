package org.example.transfer;

public class BasicBreak {
    public static void main(String[] args) {
        while (true) {
            System.out.println("inside");
            break;
        }
        System.out.println("outside & done");
    }
}

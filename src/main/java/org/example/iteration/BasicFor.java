package org.example.iteration;

class BasicFor {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
        }
        System.out.println("=======");

        for (int i = 0; i < 10;) {
            System.out.println(i);
            i++;
        }
        System.out.println("done!");
    }
}

class SpecialFor{
    public static void main(String[] args) {
        for (;;) {
            System.out.println("running...");
        }
    }
}

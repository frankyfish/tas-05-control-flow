package org.example.selection;

class IfStatement {
    public static void main( String[] args ) {
        // empty statement
        boolean execute = false;
        if(execute);
        System.out.println( "executing..." );

        // empty block
        if (true) {}
    }

    public static boolean check(int value) {
        if (value > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean anotherCheck(int value) {
        return value > 0 ? true : false;
//        return value > 0;
    }
}

class Ternary {
    public static void main(String[] args) {
        System.out.println(ternaryHell(3));
    }

    public static int ternaryHell(int value) {
        return value > 1 ?
                value < 3 ? 2
                        : 100
                : 1; // when you are fan of one-liners
        /* value > 1 ?
                value < 3 ? 2
                        : 100
                : 1; // when you are fan of one-liners
         */
    }
}

package org.example.selection;

public class ElseIfStatement {
    public static void main(String[] args) {
        long answer = 42L;

        if (answer == 404L) {
            System.out.println("not found");
        } else if (answer == 4_8_15_16_23_42L) { // actually 4815162342
            System.out.println("we're lost");
        } else if (false) {
            System.out.println("I'm here just for fun!!! =)");
        } else {
            System.out.println("Life, the Universe and Everything");
        }
    }
}

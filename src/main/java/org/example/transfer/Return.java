package org.example.transfer;

import java.util.Random;

public class Return {
    public static void main(String[] args) {
        returnNothing();
        System.out.println(returnInt());
    }

    private static void returnNothing() {
        System.out.println("inside - returnNothing");
        return;
//        System.out.println("inside - returnNothing1");
    }

    private static int returnInt() {
        return new Random().nextInt(10);
    }
}
